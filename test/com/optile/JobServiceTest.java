/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.optile;

import com.optile.callables.SendEmail;
import com.optile.callables.FileIndexing;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;

/**
 *
 * @author eugeneansah
 */
public class JobServiceTest {

    @Test
    public void jobProcess() {
//        Job job = new Job("SEND MONEY", 3000);
//        Job job1 = new Job("PAY BILLS", 0);
//        Job job2 = new Job("AIRTIME", 1000);
        SendEmail email = new SendEmail("EUGENE", "Successful Payment", "Transaction was successfull", "eugene@gmail.com", 0);
        SendEmail email1 = new SendEmail("JOSEPH", "Successful Payment", "Transaction was successfull", "joseph@gmail.com", 0);
        SendEmail email2 = new SendEmail("MAAME", "Successful Payment", "Transaction was successfull", 0);

        FileIndexing fileIndexing = new FileIndexing(20, "FILE A");
        FileIndexing fileIndexing1 = new FileIndexing(5, "FILE B");
        JobService jobService = new JobService();
        Map<String, Job> jobMap = new HashMap<>();
        jobMap.put(email.getJobName(), email);
        jobMap.put(email1.getJobName(), email1);
        jobMap.put(email2.getJobName(), email2);
        jobMap.put(fileIndexing.getJobName(), fileIndexing);
        jobMap.put(fileIndexing1.getJobName(), fileIndexing1);

//        List<Job> jobs = new ArrayList<>();
//        jobs.add(job);
//        jobs.add(job1);
//        jobs.add(job2);
        jobService.jobProcessor(jobMap);
    }

    @Test
    public void sendEmail() {
        
    }
}
