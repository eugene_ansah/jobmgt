/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.optile.callables;

import com.optile.Job;
import com.optile.enums.JobStatus;
import java.util.concurrent.Callable;

/**
 *
 * @author eugeneansah
 */
public class FileIndexing extends Job implements Callable {

    public FileIndexing(long delayTime, String name) {
        this.delayTime = delayTime;
        this.jobName = name;
    }

    @Override
    public FileIndexing call() throws Exception {
        try {
//            Thread.sleep(delayTime);
            jobStatus = JobStatus.RUNNING;
            //send email 
            jobStatus = JobStatus.SUCCESS;
             System.out.println("completed " + jobName);
        } catch (Exception e) {
            jobStatus = JobStatus.FAILED;
        }
        return this;
    }

}
