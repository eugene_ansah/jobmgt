/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.optile.callables;

import com.optile.services.EmailProcessor;
import com.optile.Job;
import com.optile.enums.JobStatus;
import com.optile.model.EmailResponse;
import java.util.concurrent.Callable;

/**
 *
 * @author eugeneansah
 */
public class SendEmail extends Job implements Callable {

    String sender;
    String title;
    String content;
    String receipient;

    public SendEmail(long delayTime, String name) {
        this.delayTime = delayTime;
        this.jobName = name;
    }

    public SendEmail(String sender, String title, String content, String receipient, long delayTime) {
        this.sender = sender;
        this.title = title;
        this.content = content;
        this.receipient = receipient;
        this.jobName = sender;
    }

    public SendEmail(String sender, String title, String content, long delayTime) {
        this.sender = sender;
        this.title = title;
        this.content = content;
        this.delayTime = delayTime;
        this.jobName = sender;
    }

    @Override
    public SendEmail call() throws Exception {
        jobStatus = JobStatus.RUNNING;
//        Thread.sleep(delayTime);
        try {
            //send email 
            EmailResponse response = EmailProcessor.sendEmail(title, content, sender, receipient);
            jobStatus = JobStatus.SUCCESS;
            if ("000".equals(response.getResponseCode())) {
                jobStatus = JobStatus.SUCCESS;
                System.out.println(jobStatus + " email to " + sender);
            } else {
                jobStatus = JobStatus.FAILED;
                System.out.println(jobStatus+ " email to " + sender);
            }
        } catch (Exception e) {
            jobStatus = JobStatus.FAILED;
        }
        return this;
    }

}
