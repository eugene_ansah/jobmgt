/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.optile.model;

/**
 *
 * @author eugeneansah
 */
public class EmailRequest {
    private String title;
    private String content;
    private String receipient;
    private String sender;

    //<editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getContent() {
        return content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    public String getReceipient() {
        return receipient;
    }
    
    public void setReceipient(String receipient) {
        this.receipient = receipient;
    }
    
    public String getSender() {
        return sender;
    }
    
    public void setSender(String sender) {
        this.sender = sender;
    }
//</editor-fold>
     
}
