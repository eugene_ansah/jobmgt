/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.optile;

import com.optile.enums.JobType;
import com.optile.enums.JobPriority;
import com.optile.enums.JobStatus;
import java.util.Date;
import java.util.concurrent.Callable;

/**
 *
 * @author eugeneansah
 */
public abstract class Job  {

    protected String jobName;
    protected String jobAction;
    protected JobType jobType;
    protected JobPriority jobPriority;
    protected JobStatus jobStatus; 
    protected long delayTime;
 
//
//    @Override
//    public Job call() throws Exception { 
////        System.out.println(jobName + " runnning");
//        Thread.sleep(delayTime); 
//        System.out.println("completed :" + jobName);
//        jobStatus = JobStatus.SUCCESS;
//        return this;
//    }
//<editor-fold defaultstate="collapsed" desc="Getters and Setters">
    
    public String getJobName() {
        return jobName;
    }
    
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    
    public String getJobAction() {
        return jobAction;
    }
    
    public void setJobAction(String jobAction) {
        this.jobAction = jobAction;
    }
    
    public JobType getJobType() {
        return jobType;
    }
    
    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }
    
    public JobPriority getJobPriority() {
        return jobPriority;
    }
    
    public void setJobPriority(JobPriority jobPriority) {
        this.jobPriority = jobPriority;
    }
    
    public JobStatus getJobStatus() {
        return jobStatus;
    }
    
    public void setJobStatus(JobStatus jobStatus) {
        this.jobStatus = jobStatus;
    }
    
    public long getDelayTime() {
        return delayTime;
    }
    
    public void setDelayTime(long delayTime) {
        this.delayTime = delayTime;
    }
//</editor-fold>
 
}
