/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.optile;

import com.optile.enums.JobStatus;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eugeneansah
 */
public class JobService {

    private ExecutorService executorService = Executors.newFixedThreadPool(4);
    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(4);
    private CompletionService<Job> completionService = new ExecutorCompletionService<>(executorService);

    List<Future<Job>> futures = new ArrayList<>();
    List<ScheduledFuture<Job>> scheduledFuture = new ArrayList<>();

    public <T extends Job> void jobProcessor(Map<String, T> jobs) {
        jobs.forEach((key, value) -> {
            value.setJobStatus(JobStatus.QUEUED);
            System.out.println("Queued " + value.jobName);
            scheduledFuture.add(scheduledExecutorService.schedule((Callable<Job>) value, value.getDelayTime(), TimeUnit.SECONDS));
        });
        scheduledFuture.forEach(f -> {
            try {
                Job j = f.get();
                System.out.println(j.getJobName() + " " + j.getJobStatus());
            } catch (InterruptedException | ExecutionException ex) {
                Logger.getLogger(JobService.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

    }
//    
//    
//    public JobService(Integer poolSize, Integer queueSize) {
//        priorityJobPoolExecutor = Executors.newFixedThreadPool(poolSize);
//        priorityQueue = new PriorityBlockingQueue<Job>(
//          queueSize, 
//          Comparator.comparing(Job::getJobPriority));
//        priorityJobScheduler.execute(() -> {
//            while (true) {
//                try {
//                    priorityJobPoolExecutor.execute(priorityQueue.take());
//                } catch (InterruptedException e) {
//                    // exception needs special handling
//                    break;
//                }
//            }
//        });
//    }

//    public void jobProcessor(Job job) {
//        priorityQueue.add(job);
//    }
}
