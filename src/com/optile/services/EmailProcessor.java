/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.optile.services;

import com.optile.model.EmailRequest;
import com.optile.model.EmailResponse;

/**
 *
 * @author eugeneansah
 */
public class EmailProcessor {

    public static EmailResponse sendEmail(String title,String content, String sender, String recipient) {
        EmailResponse response = new EmailResponse();
        try {
            if (recipient == null) {
                throw new IllegalArgumentException();
            }
            response.setResponseCode("000");
            response.setResponseMessage("Success");
        } catch (IllegalArgumentException ex) {
            response.setResponseCode("E501");
            response.setResponseMessage("No receipient supplied");
        }
        return response; 
    }
}
